Preparando seu ambiente
========================

Iniciando o projeto no Ionic
----------------------------

Crie o local para o projeto:

```sh
cd ~
mkdir desmov
cd desmov
ionic start prova3 blank
```

![primeiro_passo](img/inicio_projeto.png)

Nesse instante será criado seu projeto, quando aparecer essa pergunta abaixo marque **não**:

![ionic_appflow](img/ionic_app.png)

Pronto seu projeto foi criado, entre na pasta do projeto, nesse caso é o **prova3**

```sh
cd prova03
git config user.name "Matricula - Seu Nome"
git config user.email "Email no cest ou pessoal"
```

## Criando o projeto no GitLab
Agora abra o browser e entre no site do [GitLab](https://gitlab.com/) e crie o seu projeto de acordo com as instruções abaixo:

### Crie o projeto

Clique em Novo Projeto

![new_proj](img/gitlab_newproj.png)

Insira o nome do Projeto '**prova03**'

![proj_name](img/gitlab_projname.png)

Mude a visibilidade do projeto para '**Public**'

**Atenção**
>>>
   Importante - se o projeto não estiver visível, as questões pertinentes ao projeto terão **automaticamente** seu valor definido como '0'.
   
   Não marque a opção de criaça do README.md, deixe as opções exatamente como apresentando na imagem
>>>
![proj_visibility](img/gitlab_projvisibility.png)

Criado no Gitlab copie o endereço para adicionar no seu projeto da projeto o endereço remoto, pegue o endereço git:
1. Clique no botão clone

![proj_clonebutton](img/gitlab_clonebutton.png)

2. No campo com a URL em HTTPS, copie esse endereço completo, conforme a imagem de exemplo:

![proj_clonehttps](img/gitlab_clonehttps.png)

3. No terminal no seu projeto adicione ao repositório o endereço copiado:
```sh
git remote add origin <endereco_copiado_na_pagina_gitlab>
```

Dicas
-----
###Terminei quero saber se tem algo que possa enviar para o repositório, como faço?

1. Verifique o que foi mudado
```sh
git status
```

Vão aparecer os elementos que foram modificados ou ainda não foram adicionados ao repositório, como no exemplo abaixo:

![proj_naoadd](img/proj_naoadd.png)

Você deve observar que tem uma área de arquivos não monitorados, primeiro adicione eles com o comando 'git add', por exemplo vamos adicionar o **config.xml** que é um dos arquivos:
```sh
git add config.xml
```

Após adicionar **todos** os arquivos que precisa, faça o **commit**, por exemplo:
```sh
git commit -m 'Terminei a prova' *.*
```

Agora verifique se foi tudo certo:
```sh
git status
```

Se aparecer algo como a imagem abaixo:

![proj_falta](img/proj_statusfalta.png)

Você precisa executar um **git commit** listando cada um dos arquivos:
```sh
git commit -m 'Envio de alteracoes' arquivo1 arquivo2 arquivo3
```



